import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { Formik, Field, Form } from "formik";
import { Button, TextField } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    "& .MuiFormLabel-root": {
      color: "blue",
    },

    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderColor: "blue",
      },
      "&:hover fieldset": {
        borderColor: "#3f51b5",
      },
      "& input": {
        color: "blue",
      },
    },
  },
}));

export const MyForm = ({
  onSubmit = async (data) => console.log(JSON.stringify(data)),
}) => {
  const classes = useStyles();
  const schema = yup.object().shape({
    firstName: yup.string().required("firstName required"),
    lastName: yup.string().required("lastName required"),
    email: yup.string().email("Invalid email").required("email required"),
  });

  const { register, handleSubmit, errors, reset } = useForm({
    resolver: yupResolver(schema),
  });

  return (
    <div>
      <h1>Sign Up</h1>
      <Formik
        initialValues={{
          firstName: "",
          lastName: "",
          email: "",
        }}
        onSubmit={handleSubmit(onSubmit)}
      >
        <Form>
          <label htmlFor="firstName">First Name</label>
          <TextField
            inputProps={{ "data-testid": "firstName" }}
            className={classes.root}
            id="firstName"
            name="firstName"
            placeholder="Jane"
            inputRef={register}
            error={!!errors.firstName}
            helperText={errors.firstName?.message}
          />

          <label htmlFor="lastName">Last Name</label>
          <TextField
            inputProps={{ "data-testid": "lastName" }}
            className={classes.root}
            id="lastName"
            name="lastName"
            placeholder="Doe"
            inputRef={register}
            error={!!errors.lastName}
            helperText={errors.lastName?.message}
          />

          <label htmlFor="email">Email</label>
          <TextField
            inputProps={{ "data-testid": "email" }}
            className={classes.root}
            id="email"
            name="email"
            placeholder="jane@acme.com"
            type="email"
            inputRef={register}
            error={!!errors.email}
            helperText={errors.email?.message}
          />
          <Button type="submit">Submit</Button>
        </Form>
      </Formik>
    </div>
  );
};

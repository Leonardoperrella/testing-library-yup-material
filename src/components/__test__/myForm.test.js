import {
  render,
  waitFor,
  screen,
  fireEvent,
  act,
} from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { MyForm } from "../MyForm";

it("validates form fields", async () => {
  const { getByTestId, getByText } = render(<MyForm />);

  const firstName = getByTestId("firstName");
  const lastName = getByTestId("lastName");
  const email = getByTestId("email");

  fireEvent.blur(firstName);
  fireEvent.blur(lastName);

  userEvent.type(email, "teste");

  userEvent.click(screen.getByRole("button"));

  let firstNameError;
  let lastNameError;
  let emailError;

  await waitFor(() => {
    firstNameError = getByText("firstName required");
    lastNameError = getByText("lastName required");
    emailError = getByText("Invalid email");
  });

  expect(firstNameError).not.toBeNull();
  expect(lastNameError).not.toBeNull();
  expect(emailError).not.toBeNull();
});

it("Should validade form", async () => {
  const mockedOnSubmit = jest.fn();
  const { getByTestId } = render(<MyForm onSubmit={mockedOnSubmit} />);

  const firstName = getByTestId("firstName");
  const lastName = getByTestId("lastName");
  const email = getByTestId("email");

  await act(async () => {
    userEvent.type(firstName, "Jane");
    userEvent.type(lastName, "Doe");
    userEvent.type(email, "jane@acme.com");
  });

  await act(async () => {
    userEvent.click(screen.getByRole("button"));
  });

  expect(mockedOnSubmit).toHaveBeenCalled();
});

it("Invalid firstname", async () => {
  const { getByTestId, container } = render(<MyForm />);

  const firstName = getByTestId("firstName");

  await act(async () => {
    userEvent.click(screen.getByRole("button"));
    fireEvent.blur(firstName);
  });

  expect(container.innerHTML).toMatch("firstName required");
});

it("Invalid email", async () => {
  const { getByTestId, container } = render(<MyForm />);

  const email = getByTestId("email");

  await act(async () => {
    userEvent.type(email, "teste");
    userEvent.click(screen.getByRole("button"));
  });

  expect(container.innerHTML).toMatch("Invalid email");
});
